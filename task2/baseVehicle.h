#pragma once
#include "Cargo.h"


class baseVehicle
{
public:
	float maxWeight;

	baseVehicle(float weight) {
		maxWeight = weight;
	}

	virtual ~baseVehicle() = default;

	virtual bool checkForCargo(Cargo value) {
		return (this->maxWeight >= value.weight) ;
	}

	virtual void print();
};


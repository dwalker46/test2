#pragma once
#include "baseVehicle.h"
class vehicleType2 :
	public baseVehicle
{
	
public:
	float maxVolume;
	vehicleType2(float weight, float volume) : baseVehicle(weight) {
		maxVolume = volume;
	}

	void print() override {
		std::cout << "VH-> weight:" << this->maxWeight << ", volume: " << this->maxVolume << std::endl;
	};

	bool checkForCargo(Cargo value) override {
		bool res = baseVehicle::checkForCargo(value);
		return (!res) ? false : maxVolume >= value.volume;
	}
};


﻿#include <iostream>
#include <vector>
#include <algorithm>

#include "baseVehicle.h"
#include "vehicleType2.h"
#include "Cargo.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "");

	vector<baseVehicle*> park;
	vector<baseVehicle*> allowed;

	auto currentCargo = Cargo();
	currentCargo.volume = 2000;
	currentCargo.weight = 1800;

	allowed.clear();
	park.push_back(new baseVehicle(1200.0));
	park.push_back(new baseVehicle(1000.0));
	park.push_back(new baseVehicle(3400.0));
	park.push_back(new baseVehicle(1500.0));
	park.push_back(new vehicleType2(1500.0, 2800.0));
	park.push_back(new vehicleType2(2500.0, 2100.0));
	park.push_back(new vehicleType2(2800.0, 2100.0));

	for (const auto& o : park) {
		if (o->checkForCargo(currentCargo))
		{
			allowed.push_back(o);
			o->print();
		}
	}
	park.clear();
	park = allowed;
}
